//
//  DurationPicker.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 29/02/2024.
//

import SwiftUI

struct DurationPicker: View {
    @State var hourSelection = 0
    @State var minuteSelection = 0
    @Binding var isShowing: Bool
    @Binding var duration: Duration
    
    private var toSeconds: Int {
        ((hourSelection * 3600) + (minuteSelection * 60))
    }
    
    var durationSelection: Duration {
        Duration(secondsComponent: Int64(toSeconds), attosecondsComponent: 0)
    }
    
    static private let maxHours = 24
    static private let maxMinutes = 60
    private let hours = [Int](0...Self.maxHours)
    private let minutes = [Int](0...Self.maxMinutes)
    
    var body: some View {
        
            Button("Done") {
                isShowing = false
                duration = durationSelection
            }
            .padding(.top)
            
        GeometryReader { geometry in
            HStack(spacing: .zero) {
                Picker(selection: $hourSelection, label: Text("")) {
                    ForEach(hours, id: \.self) { value in
                        Text(value != 1 ? "\(value) hours" : "\(value) hour")
                            .tag(value)
                    }
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width / 2, alignment: .center)
                
                Picker(selection: $minuteSelection, label: Text("")) {
                    ForEach(minutes, id: \.self) { value in
                        Text(value != 1 ? "\(value) minutes" : "\(value) minute")
                            .tag(value)
                    }
                    .frame(maxWidth: .infinity, alignment: .center)
                }
                .pickerStyle(.wheel)
                .frame(width: geometry.size.width / 2, alignment: .center)
            }
        }
        
        HStack(spacing: 5) {
            Text(hourSelection == 1 ? "\(hourSelection) Hour" : "\(hourSelection) Hours")
                .font(.largeTitle)
                .fontWeight(.bold)
            Text(":")
                .font(.largeTitle)
                .fontWeight(.bold)
            Text(minuteSelection == 1 ? (minuteSelection < 10 ? "0\(minuteSelection) Minute" : "\(minuteSelection) Minute") : minuteSelection < 10 ? "0\(minuteSelection) Minutes" : "\(minuteSelection) Minutes")
                .font(.largeTitle)
                .fontWeight(.bold)
        }
        
        .frame(minHeight: 100)
    }
    
}


#Preview {
    DurationPicker(
        isShowing: .constant(true), duration: .constant(mockTask.duration))
}
