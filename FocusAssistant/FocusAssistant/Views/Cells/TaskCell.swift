//
//  TaskCell.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

struct TaskCell: View {
    let task: focusAssistant.Task
    
    
    var body: some View {
        HStack(spacing: 20) {
            Image(systemName: task.imageURL ?? "note.text")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
                .foregroundStyle(.white)
            
            VStack(alignment: .leading) {
                Text(task.name)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .foregroundStyle(.white)
                    .frame(alignment: .leading)
                if task.pomodoro {
                    Text("Pomodoro Task")
                        .foregroundStyle(.white)
                } else {
                    Text("\(task.duration.toString) - \(task.startTime!.formatted(date: .omitted, time: .shortened))")
                }
            }
            Spacer()
            
            if task.pomodoro {
                Image(systemName: "repeat.circle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 20)
                    .foregroundStyle(.white)
                Text(String(task.pomodoroCounter!))
                    .foregroundStyle(.white)
            } else {
                WeightingIndicator(weight: task.priority)
                    .frame(alignment: .trailing)
            }
           
        }
        .contentShape(Rectangle())
    
    }
}

struct ActiveTaskCell: View {
    @EnvironmentObject var vm: ActiveTaskViewModel
    
    let task: focusAssistant.Task
    
    var body: some View {
        HStack(spacing: 20) {
            Image(systemName: task.imageURL ?? "note.text")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
                .foregroundStyle(.white)
            
            VStack(alignment: .leading) {
                Text(task.name)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .foregroundStyle(.white)
                    .frame(alignment: .leading)
                Text(vm.timerStringValue)
                    .foregroundStyle(.white)
            }
            Spacer()
            
            Image(systemName: "deskclock.fill")
                .resizable()
                .symbolEffect(.pulse)
                .frame(width: 20, height: 20)
                .foregroundStyle(.white)
           
        }
        
        .contentShape(Rectangle())
    
    }
}

#Preview {
    TaskCell(task:mockPomodoroTask)
}
