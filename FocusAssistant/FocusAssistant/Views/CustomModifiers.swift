//
//  CustomModifiers.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

struct StandardButtonStyle: ViewModifier {
    
    func body(content: Content) -> some View {
        content
            .padding()
            .background(.faPurple)
            .foregroundStyle(.white)
            .clipShape(Capsule())
            .controlSize(.large)
    }
}

extension View {
    func standardButtonStyle() -> some View {
        self.modifier(StandardButtonStyle())
    }
}
