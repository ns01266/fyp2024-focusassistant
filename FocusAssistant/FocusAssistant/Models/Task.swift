//
//  Task.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import UIKit

protocol CustomTask {
    
}

struct focusAssistant {
    struct Task: Identifiable, Codable, CustomTask {
        
        var id = UUID()
        
        var name = ""
        var duration = Duration(secondsComponent: 0, attosecondsComponent: 0)
        var startTime: Date?
        var priority: Priority = .low
        var imageURL: String?
        var details: String?
        var pomodoro = false
        var isCompleted = false
        var pomodoroCounter: Int?
        var isExpired = false
        private(set) var blended: Bool = false
        
        mutating func increaseCounter() {
            if pomodoro {
                pomodoroCounter! += 1
                print("incremented")
                
                if pomodoroCounter == 4 {
                    isCompleted = true
                }
            }
        }
        
    }
}


extension focusAssistant.Task: Equatable {
    static func == (lhs: focusAssistant.Task, rhs: focusAssistant.Task) -> Bool {
        // Check if both tasks are pomodoro tasks
        if lhs.pomodoro && rhs.pomodoro {
            // If both tasks have pomodoro set to true, compare pomodoroCounter
            return lhs.pomodoroCounter == rhs.pomodoroCounter
        } else {
            // For other cases, compare all relevant properties for equality
            return lhs.id == rhs.id &&
            lhs.name == rhs.name &&
            lhs.duration == rhs.duration &&
            lhs.startTime == rhs.startTime &&
            lhs.priority == rhs.priority &&
            lhs.imageURL == rhs.imageURL &&
            lhs.details == rhs.details &&
            lhs.pomodoro == rhs.pomodoro &&
            lhs.isCompleted == rhs.isCompleted &&
            lhs.pomodoroCounter == rhs.pomodoroCounter
        }
    }
}

enum Priority: String, CaseIterable, Identifiable, Codable {
    case low
    case medium
    case high
    var id: Self { self }
}

//Samples
var sampleTasks: [focusAssistant.Task] = [ focusAssistant.Task(name: "Brush Teeth", duration: .seconds(120), startTime: Date.now.customFutureDate(daysAhead: 2), priority: .medium, imageURL: "drop.fill"),
                                           focusAssistant.Task(name: "Shower", duration: .seconds(300), startTime: Date.now.customFutureDate(daysAhead: 1), priority: .medium, imageURL: "shower.fill"),
                                           mockPomodoroTask,
                                           focusAssistant.Task(name: "Brush Teeth", duration: .seconds(120), startTime: Date.now.customFutureDate(daysAhead: 3), priority: .medium, imageURL: "drop.fill"),
                                           focusAssistant.Task(name: "Shower", duration: .seconds(300), startTime: Date.now.customFutureDate(daysAhead: 4), priority: .medium, imageURL: "shower.fill"),
                                           mockBlendedTask2.task, 
                                           mockBlendedTask.task,
                                           focusAssistant.Task(name: "Brush Teeth", duration: .seconds(120), startTime: Date.now, priority: .medium, imageURL: "drop.fill"),
                                           focusAssistant.Task(name: "Shower", duration: .seconds(300), startTime: Date.now.customFutureDate(daysAhead: 6), priority: .medium, imageURL: "shower.fill"),
                                           mockPomodoroTask,
                                           focusAssistant.Task(name: "Brush Teeth", duration: .seconds(120), startTime: Date.now.customFutureDate(daysAhead: 7), priority: .medium, imageURL: "drop.fill"),
                                           focusAssistant.Task(name: "Shower", duration: .seconds(300), startTime: Date.now, priority: .high, imageURL: "shower.fill"),
                                           mockPomodoroTask
]

var mockTask = focusAssistant.Task(name: "Shower", duration: .seconds(300), startTime: Date.now.customFutureDate(daysAhead: 2), priority: .medium, imageURL: "shower.fill", details: "Don't forget to put the shampoo back in the cabinet. Remember to clean sink after brushing teeth")

var mockPomodoroTask = focusAssistant.Task(name: "Work on assignment", priority: .medium, imageURL: "book.pages.fill", details: "Don't forget to save file after each change you make in the IDE", pomodoro: true, pomodoroCounter: 0)
