//
//  User.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 02/03/2024.
//

import Foundation
import UserNotifications
import SwiftData

@Observable final class User {
    var firstName = ""
    var lastName = ""
    var tasks: [focusAssistant.Task] = []
    var blendedTasks: [BlendedTask] = []
    var alertItem: AlertItem?
    private var clashingTask: focusAssistant.Task?
    var isEmpty: Bool {
        if firstName == "" && lastName == "" && tasks.isEmpty { true } else { false }
    }
    
    init(firstName: String = "", lastName: String = "", tasks: [focusAssistant.Task] = [], blendedTasks: [BlendedTask] = []) {
        self.firstName = firstName
        self.lastName = lastName
        self.tasks = tasks
        self.blendedTasks = blendedTasks
    }
    
    func addTask(_ task: focusAssistant.Task) {
        self.tasks.append(task)
        self.writeData()
        
        scheduleNotification(for: task)
    }
    
    func addBlendedTask(_ task: BlendedTask) {
        self.blendedTasks.append(task)
        self.addTask(task.task)
        self.writeData()
        
    }
    
    func fetchTasks() {
        self.tasks = readData().tasks
    }
    
    func deleteTask(_ taskToDelete: focusAssistant.Task) {
        for index in self.tasks.indices {
            if self.tasks[index].id == taskToDelete.id {
                let task = self.tasks[index]
                self.tasks.remove(at: index)
                descheduleNotification(for: task)
                break
            }
        }
        self.writeData()
    }
    
    func getBlendedTask(_ task: focusAssistant.Task) -> BlendedTask? {
        let blendedIDs = Set(blendedTasks.map { $0.id })

        if blendedIDs.contains(task.id) {
            return blendedTasks.first { $0.id == task.id }
        }
        return nil
    }

    
    func deleteBlendedTask(_ taskToDelete: BlendedTask) {
        guard let index = blendedTasks.firstIndex(where: { $0 == taskToDelete }) else {
            return
        }

        self.blendedTasks.remove(at: index)

        // Find the corresponding task in self.tasks array
        if let taskIndex = self.tasks.firstIndex(where: { $0.id == taskToDelete.id }) {
            let taskToDelete = self.tasks[taskIndex]
            self.deleteTask(taskToDelete)
        }

        self.writeData()
    }
    
    func checkTimeClash(_ newTask: focusAssistant.Task) {
        self.clashingTask = nil
        self.alertItem = nil
        
        if newTask.pomodoro {
            return
        } else {
            let availableTasks = self.tasks.filter { task in
                if !task.isCompleted {
                    return true
                } else {
                    return false
                }
            }
            
            for task in availableTasks {
                if !task.pomodoro {
                    let endTime = task.startTime!.addingTimeInterval(Double(task.duration.components.seconds))
                    if newTask.startTime!.isDate(inRange: task.startTime!, endDate: endTime) {
                        clashingTask = task
                        alertItem = AlertContext().taskClash(task)
                    }
                }
            }
        }
        
    }
    
    func updateTask(_ taskToUpdate: focusAssistant.Task) {
        for index in self.tasks.indices {
            if self.tasks[index].id == taskToUpdate.id { self.tasks[index] = taskToUpdate }
        }
        self.writeData()
    }
    
    func retrieveTask(_ taskToGet: focusAssistant.Task) -> focusAssistant.Task {
        var outTask = focusAssistant.Task()
        for task in self.tasks {
            if taskToGet.id == task.id {
                outTask = task
            } else { outTask = taskToGet}
        }
        return outTask
    }
    
    func completedTasksCount() -> Int {
        var count = 0
        for task in tasks {
            if task.isCompleted { count += 1 }
        }
        return count
    }
    
    func expiredTasksCount() -> Int {
        var count = 0
        for task in tasks {
            if task.isExpired { count += 1 }
        }
        return count
    }
    
    private func scheduleNotification(for task: focusAssistant.Task) {
        guard !task.pomodoro, let startTime = task.startTime, startTime > Date() else { return }
        
        let timeDifference = startTime.timeIntervalSinceNow
          guard timeDifference >= 300 else {
              // If the start time is less than 5 minutes away, don't schedule the notification
              return
          }
        
        let notificationTime = startTime.addingTimeInterval(-300) // 5 minutes before start time
        let content = UNMutableNotificationContent()
        content.title = "Task Reminder"
        content.body = "Your task \(task.name) is starting in 5 minutes!"
        content.sound = UNNotificationSound.default
        content.interruptionLevel = .timeSensitive
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: notificationTime.timeIntervalSinceNow, repeats: false)
        
        let request = UNNotificationRequest(identifier: task.id.uuidString, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { error in
            if let error = error {
                print("Error scheduling notification: \(error.localizedDescription)")
            } else {
                print("Notification scheduled for task \(task.name) at \(notificationTime.formatted(date: .omitted, time: .shortened))")
            }
        }
    }
    
    private func descheduleNotification(for task: focusAssistant.Task) {
            let notificationCenter = UNUserNotificationCenter.current()
            
            // Remove the notification request associated with the task ID
            notificationCenter.removePendingNotificationRequests(withIdentifiers: [task.id.uuidString])
        }
    
    func clearCompleted() {
        for task in self.tasks {
            if task.isCompleted {
                deleteTask(task)
                writeData()
            }
        }
    }
}

extension User: Codable {
    enum CodingKeys: CodingKey {
        case firstName, lastName, email, tasks, blendedTasks
    }
    
    convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let firstName = try container.decode(String.self, forKey: .firstName)
        let lastName = try container.decode(String.self, forKey: .lastName)
        let tasks = try container.decode([focusAssistant.Task].self, forKey: .tasks)
        let blendedTasks = try container.decode([BlendedTask].self, forKey: .blendedTasks)
        
        self.init(firstName: firstName, lastName: lastName, tasks: tasks, blendedTasks: blendedTasks)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(tasks, forKey: .tasks)
        try container.encode(blendedTasks, forKey: .blendedTasks)
    }
    
    func writeData() {
        do {
            let fileURL = try FileManager.default
                .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent("pastData.json")
            
            try JSONEncoder()
                .encode(self)
                .write(to: fileURL)
        } catch {
            alertItem = AlertContext.invalidResponse
        }
    }

    func readData() -> User {
        do {
            let fileURL = try FileManager.default
                .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("pastData.json")
            
            let data = try Data(contentsOf: fileURL)
            let pastData = try JSONDecoder().decode(User.self, from: data)
            
            return pastData
        } catch {
            self.alertItem = AlertContext.invalidUserData
            return self
        }
    }
    
    func deleteUser() {
        do {
            let fileURL = try FileManager.default
                .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                .appendingPathComponent("pastData.json")
            
            try FileManager.default.removeItem(at: fileURL)
            self.firstName = ""
            self.lastName = ""
            self.blendedTasks = []
            self.tasks = []
            
        } catch {
            alertItem = AlertContext.invalidResponse
        }
    }
    
}

extension User: Equatable {
    static func == (lhs: User, rhs: User) -> Bool {
        return lhs.tasks.count == rhs.tasks.count
    }
}

var mockUser = User(firstName: "John", lastName: "Doe", tasks: sampleTasks, blendedTasks: sampleBlendedTasks)
