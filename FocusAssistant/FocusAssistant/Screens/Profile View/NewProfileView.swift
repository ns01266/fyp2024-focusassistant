//
//  NewProfileView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 02/03/2024.
//

import SwiftUI

struct NewProfileView: View {
    
    @Environment(User.self) var user
    
    @State var viewModel = ProfileViewModel()
    @FocusState private var focusedTextField: FormTextField?
    @Environment(\.dismiss) private var dismiss
    
    var newProfile = true
    
    enum FormTextField {
        case firstName, lastName
    }
    

    
    var body: some View {
        NavigationStack {
            Form {
                Section(header: Text("Personal Info")) {
                    TextField("First Name", text: $viewModel.user.firstName)
                        .autocorrectionDisabled(true)
                        .focused($focusedTextField, equals: .firstName)
                        .onSubmit {focusedTextField = .lastName}
                        .submitLabel(.next)
                    
                    TextField("Last Name", text: $viewModel.user.lastName)
                        .autocorrectionDisabled(true)
                        .focused($focusedTextField, equals: .lastName)
                        .submitLabel(.done)
                        
                    Button {
                        viewModel.saveChanges()
                        dismiss()
                    } label: {
                        Text("Save Changes")
                    }
                }
            }
            .navigationTitle(newProfile ? "New Profile" : "Edit Profile")
            .toolbar {
                ToolbarItemGroup(placement: .keyboard) {
                    Button("Dismiss") { focusedTextField = nil }
                }
                
                ToolbarItem(placement: .topBarLeading) {
                    Button {
                        dismiss()
                    } label: { Text("Back") }
                }
            }
            .onAppear {
                viewModel.setup(user)
            }
            
            .background{
                Color(.BG)
                    .ignoresSafeArea()
            }
        }
    }
}

#Preview {
    NewProfileView()
}
