//
//  ProfileView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

struct ProfileView: View {
    
    @Environment(User.self) var user
    
    @State var viewModel = ProfileViewModel()
    @State var isDisplayingNewProfile = false
    @State var isDisplayingEditProfile = false
    
    enum FormTextField {
        case firstName, lastName, email
    }
    
    var body: some View {
        NavigationStack {
            ZStack {
                Color.BG
                    .ignoresSafeArea()
                
                VStack {
                    if !viewModel.user.isEmpty {
                        Text("Welcome back, \(viewModel.user.firstName)")
                            .font(.title)
                        
                        Button {
                            viewModel.setup(user)
                            isDisplayingEditProfile = true
                        } label: {
                            Text("Edit Profile")
                        }
                        
                        Button {
                            user.deleteUser()
                            viewModel.setup(user)
                        } label: {
                            Text("Delete Profile")
                                .foregroundStyle(.red)
                        }
                    } else {
                        ContentUnavailableView("You haven't created a profile yet", systemImage: "person.slash.fill", description: Text("Press the button to get started"))
                        
                        Button {
                            isDisplayingNewProfile = true
                        } label: {
                            Text("Create new Profile")
                        }
                        .buttonStyle(.borderedProminent)
                        .padding(.bottom)
                    }
                }
                .navigationTitle("Profile")
                
                .sheet(isPresented: $isDisplayingNewProfile, onDismiss: {viewModel.retrieveUser()}, content: {
                    NewProfileView()
                })
                
                .sheet(isPresented: $isDisplayingEditProfile, onDismiss: {viewModel.retrieveUser()}, content: {
                    NewProfileView(newProfile: false)
            })
            }
        }
        .onAppear{
            viewModel.setup(user)
        }
        .alert(item: $viewModel.alertItem) { alertItem in
            Alert(title: alertItem.title,
                  message: alertItem.message,
                  dismissButton: alertItem.dismissButton)
        }
    }
}

#Preview {
    ProfileView().environment(User())
}

