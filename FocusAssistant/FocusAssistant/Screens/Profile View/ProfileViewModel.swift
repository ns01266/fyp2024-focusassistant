//
//  ProfileViewModel.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 02/03/2024.
//

import SwiftUI

@Observable final class ProfileViewModel {
    
    var user = User()
    var alertItem: AlertItem?
    var profileExists: Bool = false
   
    
    var isValidForm: Bool {
        guard !user.firstName.isEmpty && !user.lastName.isEmpty else {
            alertItem = AlertContext.invalidForm
            return false
        }
        
        return true
    }
    
    func setup(_ user: User) {
        self.user = user
        retrieveUser()
      }
    
    func saveChanges() {
        user.writeData()
    }
    
    func retrieveUser() {
        self.user = user.readData()
        
        if user.alertItem != nil && user.alertItem != AlertContext.invalidUserData {
            profileExists = false
            alertItem = user.alertItem
        } else if user.alertItem == AlertContext.invalidUserData {
            profileExists = false
        } else {
            profileExists = true
        }
        
    }
}
