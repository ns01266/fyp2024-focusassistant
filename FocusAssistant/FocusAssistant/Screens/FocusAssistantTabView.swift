//
//  ContentView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI
import SwiftOpenAI

struct FocusAssistantTabView: View {
    @Environment(User.self) var user
    @EnvironmentObject var activeTaskModel: ActiveTaskViewModel
    @Environment(\.dismiss) private var dismiss
    
    var body: some View {
        TabView {
            HomeView()
                .tabItem { Label("Home", systemImage: "house") }
            
            TaskView()
                .tabItem { Label("Tasks", systemImage: "note.text") }
            
            BlenderView(service: OpenAIServiceFactory.service(apiKey: APIKey.default))
                .tabItem { Label("Task Blender", systemImage: "tornado") }
            
            ProfileView()
                .tabItem { Label("Profile", systemImage: "person") }
            
            
        }
        .alert(activeTaskModel.alertMessage, isPresented: $activeTaskModel.isFinished) {
            if activeTaskModel.isFinished {
                if let activeTask = activeTaskModel.activeTask, !activeTaskModel.isShowing {
                    if activeTask.pomodoro {
                        if shouldStartBreak() {
                            startBreakButtons()
                        } else if activeTaskModel.isBreak {
                            startTaskButtons()
                        } else {
                            completeTaskButton()
                        }
                    } else {
                        startNewTaskButtons()
                    }
                }
            }
            
        }
        .onChange(of: activeTaskModel.isFinished) {
            user.fetchTasks()
        }

    }
    
    func shouldStartBreak() -> Bool {
        return activeTaskModel.activeTask!.pomodoroCounter! < 4 && !activeTaskModel.isBreak
    }
    
    func startBreakButtons() -> some View {
       Group {
           Button("Start Break", role: .cancel) {
               activeTaskModel.isBreak = true
               activeTaskModel.startPomodoroBreak()
           }
           Button("Close", role: .destructive) {
               activeTaskModel.endTimer()
               dismiss()
           }
           Button("Complete task", role: .destructive) {
               activeTaskModel.endTimer()
               activeTaskModel.activeTask!.isCompleted = true
               activeTaskModel.activeTask = nil
               dismiss()
           }
       }
   }

    @ViewBuilder
    func startTaskButtons() -> some View {
       Group {
           Button("Start Task", role: .cancel) {
               activeTaskModel.isBreak = false
               activeTaskModel.setActiveTask(activeTaskModel.activeTask!)
               activeTaskModel.startTimer()
           }
           Button("Close", role: .destructive) {
               activeTaskModel.endTimer()
               dismiss()
           }
       }
   }

    @ViewBuilder
    func completeTaskButton() -> some View {
       Button("Complete task", role: .destructive) {
           activeTaskModel.endTimer()
           activeTaskModel.activeTask!.isCompleted = true
        
           dismiss()
       }
   }

    @ViewBuilder
    func startNewTaskButtons() -> some View {
       Group {
           Button("Start New", role: .cancel) {
               activeTaskModel.endTimer()
               activeTaskModel.addNewTimer = true
           }
           Button("Close", role: .destructive) {
               activeTaskModel.endTimer()
               activeTaskModel.activeTask!.isCompleted = true
               user.updateTask(activeTaskModel.activeTask!)
               activeTaskModel.activeTask = nil
               dismiss()
           }
       }
   }

    
}

#Preview {
    FocusAssistantTabView()
}
