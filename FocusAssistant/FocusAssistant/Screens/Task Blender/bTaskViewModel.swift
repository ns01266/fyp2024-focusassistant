//
//  bTaskViewModel.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 06/04/2024.
//

import Foundation

@Observable final class bTaskViewModel {
    var user = User()
    var isAnimated = false
    
    func setup(_ user: User) {
        self.user = User()
        self.user = user.readData()
    }
}
