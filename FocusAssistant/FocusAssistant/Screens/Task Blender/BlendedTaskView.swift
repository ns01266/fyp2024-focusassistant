//
//  BlendedTaskView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 02/04/2024.
//

import SwiftUI

struct BlendedTaskView: View {
    @Environment(User.self) var user
    
    @State var blendedTask: BlendedTask?
    
    @Binding var isShowing: Bool
    @Binding var reset: Bool
    

    @State var vm = bTaskViewModel()
    
    var body: some View {
        ZStack {
            Color.BG.ignoresSafeArea(.all)
            
            if vm.user.isEmpty {
                ContentUnavailableView("There was an issue retrieving your profile", systemImage: "person.slash.fill", description: Text("Reload the page and try again"))
            } else {
                if let task = blendedTask {
                    VStack {
                        Text("Task: " + task.name)
                            .font(.title)
                            .fontWeight(.semibold)
                            .frame(alignment: .center)
                            .padding(.top)
                            .padding(.horizontal, 5)
                        
                        List {
                            ForEach(Array(task.subtasks.enumerated()), id: \.1.id) { index, subtask in
                                SubtaskListItem(subtask: subtask, taskNo: index + 1)
                                    .transition(AnyTransition.slide)
                                    .animation(Animation.easeInOut.delay(Double(index) * 0.1), value: vm.isAnimated)
                                    .frame(maxWidth: .infinity, alignment: .center)
                            }
                            .listRowBackground(Color.faPurple)
                            .onAppear {
                                withAnimation { vm.isAnimated = true }
                            }
                        }
                        .listStyle(.automatic)
                        .listRowSpacing(10)
                        .scrollContentBackground(.hidden)
                        
                        Spacer()
                        
                        HStack {
                            Button("Create Task") {
                                vm.user.addBlendedTask(task)
                                isShowing = false
                            }
                            .buttonStyle(.borderedProminent)
                            
                            Button("Reset response", role: .destructive) {
                                reset = true
                                isShowing = false
                            }
                            .buttonStyle(.borderedProminent)
                        }
                        .padding(.vertical)
                    }
                    .padding()
                } else {
                    ContentUnavailableView("Error showing task", systemImage: "tag.slash.fill", description: Text("Try generating task again"))
                }
            }
        }
        .onAppear {
            vm.setup(user)
        }
        .overlay(alignment: .topTrailing, content: {
            Button(action: { isShowing = false }, label: {
                XDismissButton()
            })
            .padding(3)
            .buttonStyle(.automatic)
        })
        .frame(width: 320, height: 565)
        .cornerRadius(12)
        .shadow(radius: 40)
    }
}

struct SubtaskListItem: View {
    let subtask: Subtask
    let taskNo: Int
    @State private var isExpanded: Bool = false
    @State private var isAnimated = false
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: {
                withAnimation {
                    isExpanded.toggle()
                }
            }) {
                Text("Task \(taskNo): \(subtask.name)")
                    .font(.headline)
                    .foregroundStyle(.white)
            }
            
            if isExpanded {
                
                ForEach(0...subtask.details.count - 1, id: \.self) { index in
                    VStack(alignment: .leading, spacing: 5) {
                        Text(subtask.details[index].description)
                            .padding(.horizontal, 10)
                            .padding(.vertical, 5)
                            .background(Color.darkPurple)
                            .cornerRadius(8)
                            .foregroundStyle(.white)
                    }
                    .shadow(radius: 5)
                    .transition(AnyTransition.slide)
                    .animation(.easeInOut, value: isAnimated)
                }
            }
        }
        .padding(.vertical, 5)
    }
}

#Preview{
    BlendedTaskView(blendedTask: mockBlendedTask, isShowing: .constant(true), reset: .constant(false))
        .environment(mockUser)
}
