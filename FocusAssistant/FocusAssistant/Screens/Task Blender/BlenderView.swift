//
//  BlenderView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 01/04/2024.
//

import SwiftUI
import SwiftOpenAI
import AsyncButton

struct BlenderView: View {
    private let service: OpenAIService
    @Bindable private var vm: BlenderViewModel
    
    
    @State private var blendingTask: Task<Void, Never>? = nil
    @State var isShowingTask = false
    @FocusState private var isFocused: Bool
    @State var blendedTask: BlendedTask?
    @State var reset = false
    
    
    init(service: OpenAIService) {
        self.service = service
        self.vm = BlenderViewModel(service: service)
    }
    
    var body: some View {
        ZStack {
            ZStack {
                Rectangle().fill(!isShowingTask ? Color(.BG) : Color(.gray))
                    .ignoresSafeArea(.all)
                if let blendedTask = vm.blendedTask {
                    Button("See details of task: \(blendedTask.name)") {
                        self.blendedTask = vm.blendedTask
                        withAnimation {
                            isShowingTask = true
                        }
                    }
                    .buttonStyle(.borderedProminent)
                    .transition(.blurReplace)
                } else {
                    VStack {
                        TextField("Enter Task e.g. Make a salad", text: $vm.userPrompt)
                            .textFieldStyle(.roundedBorder)
                            .focused($isFocused)
                            .padding()
                        
                        Button("Blend Task") {
                            isFocused = false
                            blendingTask = Task {
                                await vm.blendTask()
                            }
                        }
                        .padding()
                        .disabled(vm.isLoading ?? false)
                        
                        Button("Cancel blend", role: .destructive) {
                            blendingTask?.cancel()
                            vm.isLoading = false
                        }
                        .padding(.bottom)
                        .disabled((vm.isLoading ?? false) ? false : true)
                        .opacity((vm.isLoading ?? false) ? 100 : 0)
                        
                        if let isLoading = vm.isLoading {
                            if isLoading {
                                ProgressView("Blending...")
                            }
                        } else {
                            Text("Blender Ready to start")
                        }
                        
                    }
                    .animation(.easeInOut, value: vm.blendedTask == nil)
                }
            }
            .blur(radius: isShowingTask ? 25 : 0)
            if isShowingTask {
                BlendedTaskView(blendedTask: blendedTask, isShowing: $isShowingTask, reset: $reset)
                    .transition(.blurReplace)
                
            }
            
            
        }
        .buttonStyle(.borderedProminent)
        .animation(.bouncy, value: isShowingTask)
        .onChange(of: reset) { oldValue, newValue in
            if newValue == true {
                
                withAnimation {
                    vm.blendedTask = nil
                    blendedTask = nil
                    reset = false
                }
            }
            
        }
        
    }
    
    
}


#Preview {
    BlenderView(service: OpenAIServiceFactory.service(apiKey: APIKey.default))
}
