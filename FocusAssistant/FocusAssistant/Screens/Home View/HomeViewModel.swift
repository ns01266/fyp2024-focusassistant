//
//  HomeViewModel.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 23/03/2024.
//

import SwiftUI

@Observable final class HomeViewModel {
    var isShowingDetail = false
    var isDisplayingAddView = false
    var isShowingEditView = false
    var selectedTask: focusAssistant.Task?
    var user = User()
    
    func setup(_ user: User) {
        self.user = User()
        self.user = user.readData()
    }
}
