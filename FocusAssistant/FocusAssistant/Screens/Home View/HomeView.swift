//
//  HomeView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

struct HomeView: View {
    /// - View Properties
    @Environment(User.self) var user
    
    @State private var currentDay: Date = .init()
    @Bindable var vm = HomeViewModel()
    var body: some View {
        ZStack {
            Color.BG.ignoresSafeArea(.all)
            
            if vm.user.isEmpty {
                ContentUnavailableView("No Profile Created", systemImage: "person.slash.fill", description: Text("You don't have an active profile. Head to the profile tab to create one"))
            } else {
                ScrollView(.vertical, showsIndicators: false) {
                    TimelineView()
                        .padding(15)
                }
                .safeAreaInset(edge: .top,spacing: 0) {
                    HeaderView()
                }
                .fullScreenCover(isPresented: $vm.isShowingEditView, onDismiss: { vm.setup(user) }, content: {
                    AddTaskView(task: vm.selectedTask!)
                })
                .fullScreenCover(isPresented: $vm.isDisplayingAddView, onDismiss: { vm.setup(user) }, content: {
                    AddTaskView()
                })
                .fullScreenCover(isPresented: $vm.isShowingDetail, onDismiss: { vm.setup(user) }, content: {
                    TaskDetailView(task: vm.selectedTask!)
                })
                
                
            }
        }
        .onAppear {
            vm.setup(user)
        }
    }
    
    /// - Timeline View
    @ViewBuilder
    func TimelineView()->some View{
        ScrollViewReader { proxy in
            let hours = Calendar.current.hours
            let midHour = hours[hours.count / 2]
            VStack{
                ForEach(hours,id: \.self){hour in
                    TimelineViewRow(hour)
                        .id(hour)
                }
            }
            .onAppear {
                proxy.scrollTo(midHour)
            }
        }
        
    }
    
    /// - Timeline View Row
    @ViewBuilder
    func TimelineViewRow(_ date: Date)->some View{
        HStack(alignment: .top) {
            Text(date.toString("h a"))
                .font(.system(size: 16))
                .fontWeight(.regular)
                .frame(width: 45,alignment: .leading)
            
            /// - Filtering Tasks
            let calendar = Calendar.current
            let filteredTasks = vm.user.tasks.filter{
                if let hour = calendar.dateComponents([.hour], from: date).hour,
                   let startTime = $0.startTime,
                   let taskHour = calendar.dateComponents([.hour], from: startTime).hour,
                   hour == taskHour && calendar.isDate(startTime, inSameDayAs: currentDay) && !$0.isCompleted && !$0.isExpired {
                    return true
                }
                return false
            }
            
            if filteredTasks.isEmpty{
                Rectangle()
                    .stroke(.gray.opacity(0.5), style: StrokeStyle(lineWidth: 0.5, lineCap: .butt, lineJoin: .bevel, dash: [5], dashPhase: 5))
                    .frame(height: 0.5)
                    .offset(y: 10)
            }else{
                /// - Task View
                VStack(spacing: 10){
                    ForEach(filteredTasks){task in
                        TaskRow(task)
                            .onTapGesture {
                                vm.selectedTask = task
                                vm.isShowingDetail = true
                            }
                            .contextMenu {
                                Button("Show Details") {
                                    vm.selectedTask = task
                                    vm.isShowingDetail = true
                                }
                                
                                Button("Edit Task") {
                                    vm.selectedTask = task
                                    vm.isShowingEditView = true
                                }
                                
                                Button("Delete Task", role: .destructive) {
                                    vm.user.deleteTask(task)
                                }
                                
                            }
                    }
                }
            }
        }
        .hAlign(.leading)
        .padding(.vertical,15)
    }
    
    /// - Task Row
    @ViewBuilder
    func TaskRow(_ task: focusAssistant.Task)->some View{
        var highP: Bool {
            if task.priority == .high {
                false
            } else { true }
        }
        
        VStack(alignment: .leading, spacing: 8) {
            HStack {
                if let taskImage = task.imageURL {
                    Image(systemName: taskImage)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 22, height: 22)
                        .foregroundStyle(highP ? .faPurple : .red)
                }
                
                Text(task.name.uppercased())
                    .font(.system(size: 16))
                    .fontWeight(.regular)
                    .foregroundStyle(highP ? .faPurple : .red)
                    .lineLimit(1)
                
                
                if task.details != nil {
                    Text(task.details!)
                        .font(.system(size: 14))
                        .fontWeight(.light)
                        .foregroundStyle(highP ? .faPurple.opacity(0.8) : .red.opacity(0.8))
                        .lineLimit(6)
                }
            }
        }
        .hAlign(.leading)
        .padding(12)
        .background {
            ZStack(alignment: .leading) {
                Rectangle()
                    .fill(highP ? .faPurple : .red)
                    .frame(width: 4)
                
                Rectangle()
                    .fill(highP ? .faPurple.opacity(0.25) : .red.opacity(0.25))
            }
        }
    }
    
    /// - Header View
    @ViewBuilder
    func HeaderView()->some View{
        VStack{
            HStack{
                VStack(alignment: .leading, spacing: 6) {
                    Text("Today")
                        .font(.largeTitle)
                        .fontWeight(.light)
                    
                    Text("Welcome, \(vm.user.firstName)")
                        .font(.subheadline)
                        .fontWeight(.light)
                }
                .hAlign(.leading)
                
                Button {
                    vm.isDisplayingAddView = true
                } label: {
                    HStack(spacing: 10){
                        Image(systemName: "plus")
                        Text("Add Task")
                    }
                } .buttonStyle(.borderedProminent)
            }
            
            /// - Today Date in String
            Text(Date().toString("MMM YYYY"))
                .hAlign(.leading)
                .padding(.top,15)
            
            /// - Current Week Row
            WeekRow()
        }
        .padding(15)
        .background {
            VStack(spacing: 0) {
                Color.BG
                
                /// - Gradient Opacity Background
                Rectangle()
                    .fill(.linearGradient(colors: [
                        .BG,
                        .clear
                    ], startPoint: .top, endPoint: .bottom))
                    .frame(height: 20)
            }
            .ignoresSafeArea()
        }
    }
    
    /// - Week Row
    @ViewBuilder
    func WeekRow()->some View{
        HStack(spacing: 0){
            ForEach(Calendar.current.currentWeek){weekDay in
                let status = Calendar.current.isDate(weekDay.date, inSameDayAs: currentDay)
                VStack(spacing: 6){
                    Text(weekDay.string.prefix(3))
                        .font(.system(size: 14))
                        .fontWeight(.medium)
                    Text(weekDay.date.toString("dd"))
                        .font(.system(size: 18))
                        .fontWeight(status ? .medium : .regular)
                }
                .overlay(alignment: .bottom, content: {
                    if weekDay.isToday{
                        Circle()
                            .frame(width: 6, height: 6)
                            .offset(y: 12)
                    }
                })
                .foregroundColor(status ? Color(.faPurple) : .gray)
                .hAlign(.center)
                .contentShape(Rectangle())
                .onTapGesture {
                    withAnimation(.easeInOut(duration: 0.3)){
                        currentDay = weekDay.date
                    }
                }
            }
        }
        .padding(.vertical,10)
        .padding(.horizontal,-15)
    }
}

#Preview {
    HomeView().environment(mockUser)
}

// MARK: View Extensions
extension View{
    func hAlign(_ alignment: Alignment)->some View{
        self
            .frame(maxWidth: .infinity,alignment: alignment)
    }
    
    func vAlign(_ alignment: Alignment)->some View{
        self
            .frame(maxHeight: .infinity,alignment: alignment)
    }
}
