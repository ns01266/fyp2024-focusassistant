//
//  TaskDetailView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

struct TaskDetailView: View {
    @Environment(\.dismiss) private var dismiss
    @EnvironmentObject var vm: ActiveTaskViewModel
    @Environment(User.self) var user
    
    @State var task: focusAssistant.Task
    @State var viewModel = TaskDetailViewModel()
    @State private var isAnimated = false
    
    var body: some View {
        NavigationStack {
            ZStack {
                Color.BG.ignoresSafeArea()
                
                    VStack {
                        Image(systemName: task.imageURL ?? "note.text")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 150, height: 75)
                            .padding()
                        
                        Text(task.name)
                            .font(.title2)
                            .fontWeight(.semibold)
                        
                        Text(task.details ?? "")
                            .multilineTextAlignment(.center)
                            .font(.body)
                            .padding()
                        
                        HStack(spacing: 25) {
                            
                            if task.pomodoro {
                                TaskInfo(title: "Priority", value: String(describing: task.priority))
                                TaskInfo(title: "Times Completed", value: String(task.pomodoroCounter!))
                            } else {
                                TaskInfo(title: "Duration", value: task.duration.toString)
                                TaskInfo(title: "Priority", value: String(describing: task.priority))
                                TaskInfo(title: "Start Time", value: task.startTime!.formatted(date: .omitted, time: .shortened))
                                TaskInfo(title: "Date", value: task.startTime!.formatted(date: .abbreviated, time: .omitted))
                            }
                            
                        }
                        Spacer()
                        
                        ZStack {
                            Rectangle().fill(Color.darkPurple.opacity(0.4))
                                .clipShape(.rect(cornerRadius: 30))
                            Text(viewModel.taskStartContext ?? "")
                                .multilineTextAlignment(.leading)
                                .lineLimit(nil)
                                .padding(7)
                        }
                        .frame(maxWidth: 300, minHeight: 40, maxHeight: 85)
                        .padding()
                        .opacity(viewModel.isDisplayingContext ? 100 : 0)
                    }
                    .padding(.top, 30)
                }
                
                
                
            
        }
        .fullScreenCover(isPresented: $viewModel.isDisplayingActiveTask, content: {
            if let selectedTask = viewModel.selectedTask {
                ActiveTaskView(task: selectedTask)
            }
        })
        
        .onAppear {
            viewModel.setup(user)
            task = viewModel.user.retrieveTask(task)
        }
        .onChange(of: viewModel.isDisplayingContext, { oldValue, newValue in
            if newValue == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    withAnimation(.easeInOut(duration: 2)) {
                        viewModel.isDisplayingContext = false
                    }
                }
            }
        })
        .toolbar {
            ToolbarItem(placement: .topBarLeading) {
                Button {
                    dismiss()
                } label: { Text("Back") }
            }
            
            ToolbarItem(placement: .bottomBar) {
                Button("Start Task") {
                    viewModel.selectedTask = task
                    if task.pomodoro {
                        viewModel.isDisplayingActiveTask = true
                    } else {
                        switch task.priority {
                        case .low:
                            viewModel.isDisplayingActiveTask = true
                        case .medium:
                            if abs(task.startTime!.timeIntervalSince(Date.now)) > 600 {
                                withAnimation(.easeInOut(duration: 2)) {
                                    viewModel.isDisplayingContext = true
                                }
                            } else {
                                viewModel.isDisplayingActiveTask = true
                            }
                        case .high:
                            withAnimation(.easeInOut) {
                                viewModel.isDisplayingContext = true
                            }
                        }
                    }
                    
                    
                }
                .standardButtonStyle()
                
            }
        }
        
    }
}

#Preview {
    TaskDetailView(task: mockTask)
        .environmentObject(ActiveTaskViewModel())
        .environment(mockUser)
}


struct TaskInfo: View {
    let title: String
    let value: String
    
    var body: some View {
        VStack(spacing: 5) {
            Text(title)
                .bold()
                .font(.caption)
            Text(value)
                .fontWeight(.semibold)
                .foregroundStyle(.secondary)
                .italic()
        }
    }
}
