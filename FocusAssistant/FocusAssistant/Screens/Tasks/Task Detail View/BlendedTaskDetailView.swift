//
//  BlendedTaskDetailView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 06/04/2024.
//

import SwiftUI

struct BlendedTaskDetailView: View {
    @Environment(\.dismiss) private var dismiss
    @Environment(User.self) var user
    
    @State var name = mockBlendedTask2.name
    @State var vm = BlendedTaskViewModel()
    @State private var isAnimated = false
    
    init(blendedTask: BlendedTask) {
        self.vm = BlendedTaskViewModel()
        self.vm.subtasks = blendedTask.subtasks
    }
    
    var body: some View {
        
        ZStack {
            Color.BG.ignoresSafeArea()
            VStack {
                Text("Task: " + name)
                    .font(.title)
                    .fontWeight(.semibold)
                    .frame(alignment: .center)
                    .padding(.top)
                    .padding(.horizontal, 5)
                
                List {
                    ForEach(vm.subtasks.indices, id: \.self) { i  in
                        SubtaskCell(subtask: self.$vm.subtasks[i], taskNo: i + 1)
                    }
                    .listRowBackground(Color.faPurple)
                    .onAppear {
                        withAnimation { isAnimated = true }
                    }
                }
                .buttonStyle(BorderlessButtonStyle())
                .listRowSpacing(10)
                .scrollContentBackground(.hidden)
                
                Spacer()
                
                HStack {
                    //buttons
                    
                }
                .padding(.vertical)
            }
            .padding(.top, 30)
            .onAppear {
                vm.setup(user)
            }
            .toolbar {
                ToolbarItem(placement: .topBarLeading) {
                    Button("Back") { dismiss() }
                }
                
                ToolbarItem(placement: .bottomBar) {
                    Button("Start Task") {
                        dismiss()
                    }
                    .buttonStyle(.borderedProminent)
                }
            }
        }
        .fullScreenCover(isPresented: $vm.isDisplayingActiveTask, content: {
            if let selectedTask = vm.selectedTask {
                ActiveTaskView(task: selectedTask)
            }
        })
    
    }
    
        
}

struct SubtaskCell: View {
    @Binding var subtask: Subtask
    let taskNo: Int
    
    @State private var isAnimated = false
    
    var body: some View {
        VStack(alignment: .center) {
            Text("Task \(taskNo): \(subtask.name)")
                .font(.headline)
                .foregroundStyle(.white)
            
            
            ForEach(subtask.details.indices, id: \.self) { j in
                DetailRow(detail: $subtask.details[j])
            }
            
        }
        .padding(.vertical, 5)
        .transition(AnyTransition.slide)
        .animation(Animation.easeInOut.delay(Double(taskNo) * 0.1), value: isAnimated)
    }
}

struct DetailRow: View {
    @Binding var detail: Subtask.Detail
    @State private var isAnimated = false
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(detail.description)
            }
            Spacer()
            
            Button(action: {
                withAnimation {
                    detail.toggleCompleted()
                }
            }, label: {
                Image(systemName: detail.isCompleted ? "checkmark.circle.fill" : "circle")
                    .contentTransition(.symbolEffect(.replace))
                
            })
            .frame(width: 35, height: 35)
            .foregroundStyle(detail.isCompleted ? .green : .white)
            
        }
        .padding(.vertical, 5)
        .padding(.horizontal, 10)
        .background(Color.darkPurple)
        .cornerRadius(10)
        .foregroundStyle(.white)
        .shadow(radius: 5)
        .transition(AnyTransition.slide)
        .animation(.easeInOut, value: isAnimated)
    }
}

#Preview {
    BlendedTaskDetailView(blendedTask: mockBlendedTask2)
        .environment(mockUser)
}
