//
//  BlendedTaskViewModel.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 06/04/2024.
//

import Foundation

@Observable final class BlendedTaskViewModel {
    var user = User()
    var isDisplayingActiveTask = false
    var selectedTask: focusAssistant.Task?
    var isDisplayingContext = false
    var subtasks: [Subtask] = []
    
    func setup(_ user: User) {
        self.user = user.readData()
    }
}
