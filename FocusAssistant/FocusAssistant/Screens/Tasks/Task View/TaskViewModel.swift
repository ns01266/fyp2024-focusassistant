//
//  TaskViewModel.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI
import Observation

@Observable final class TaskViewModel {
    
    var isShowingDetail = false
    var selectedTask: focusAssistant.Task?
    var isDisplayingAddView = false
    var isShowingCompleted = false
    var isDisplayingEditView = false
    var isShowingAllTasks = false
    var isShowingDailyTasks = false
    var isShowingWeeklyTasks = false
    var isShowingCompletedTasks = false
    var isShowingExpiredTasks = false
    var isShowingActiveView = false
    var user = User()
    var status = Status.showAll
    
    
    func setup(_ user: User) {
        self.user = User()
        self.user = user.readData()
    }
    
}

enum Status {
    case showDaily, showWeekly, showAll
}
