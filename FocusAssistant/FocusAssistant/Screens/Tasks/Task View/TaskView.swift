//
//  TaskView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

struct TaskView: View {
    
    @Environment(User.self) var user
    @State var vm = TaskViewModel()
    @EnvironmentObject var activeVM: ActiveTaskViewModel
    @State private var currentDay: Date = .init()
    
    @State var activeAnimator = true
    @State var priorityAnimator = true
    var body: some View {
        NavigationStack {
            ZStack {
                Color.BG
                    .ignoresSafeArea()
                VStack {
                    if vm.user.isEmpty {
                        ContentUnavailableView("No Profile Created", systemImage: "tag.slash.fill", description: Text("You don't have an active profile. Head to the profile tab to create one"))
                    } else if vm.user.tasks.isEmpty {
                        ContentUnavailableView("No Tasks", systemImage: "tag.slash.fill", description: Text("You don't have any tasks currently"))
                    } else {
                        List {
                            if let activeTask = activeVM.activeTask, user.readData().tasks.contains(activeTask) {
                                Section("Active Task") {
                                    ActiveTaskCell(task: activeTask)
                                }
                            }
                            
                            switch vm.status {
                            case .showAll:
                                allTasksView()
                            case .showDaily:
                                dailyTasksView()
                            case .showWeekly:
                                weeklyTasksView()
                            }
                            
                            Section("Completed Tasks (\(vm.user.completedTasksCount()))", isExpanded: $vm.isShowingCompleted) {
                                ForEach(vm.user.tasks.filter { task in
                                    if task.isCompleted { true } else { false } }) {task in
                                        TaskCell(task: task)
                                }
                            }
                            
                            Section("Expired Tasks (\(vm.user.expiredTasksCount()))", isExpanded: $vm.isShowingExpiredTasks) {
                                ForEach(vm.user.tasks.filter { task in
                                    if task.isExpired { true } else { false } }) {task in
                                        TaskCell(task: task)
                                }
                            }
                            
                            
                        }
                        .listRowSpacing(10)
                        .scrollContentBackground(.hidden)
                    }
                    
                    Spacer()
                    
                    if !vm.user.isEmpty {
                        HStack {
                            Button {
                                vm.isDisplayingAddView = true
                            } label: {
                                Text("Add new task")
                            }
                            .buttonStyle(.borderedProminent)
                            .padding()
                            
                            Spacer()
                            
                            Menu(content: {
                                Button("Show All tasks", action: {
                                    vm.status = .showAll
                                })
                                .disabled(vm.status == .showAll)
                                Button("Show Weekly tasks", action: {
                                    vm.status = .showWeekly
                                }
                                )
                                .disabled(vm.status == .showWeekly)
                                Button("Show Daily tasks", action: {
                                    vm.status = .showDaily
                                })
                                .disabled(vm.status == .showDaily)
                                Button(vm.isShowingCompleted ? "Hide Completed tasks" : "Show Completed tasks", action: {vm.isShowingCompleted.toggle()})
                                
                                Button(vm.isShowingExpiredTasks ? "Hide Expired tasks" : "Show Expired tasks", action: {vm.isShowingExpiredTasks.toggle()})
                                
                                Button("Clear Completed", role: .destructive) {
                                    vm.user.clearCompleted()
                                }
                                .disabled(vm.user.completedTasksCount() < 1)
                            }, label: {
                                Image(systemName: "line.3.horizontal.circle")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: 40, height: 40)
                            })
                            .padding()
                        }
                    }
                    
                }
                .onAppear {
                    vm.setup(user)
                }
                .onChange(of: activeVM.activeTask, { oldValue, newValue in
                    vm.setup(user)
                })
                
                .fullScreenCover(isPresented: $vm.isDisplayingAddView, onDismiss: { vm.setup(user) }, content: {
                    AddTaskView()
                })
                
                .fullScreenCover(isPresented: $vm.isDisplayingEditView, onDismiss: { vm.setup(user) }, content: {
                    AddTaskView(task: vm.selectedTask!)
                })
                
                .fullScreenCover(isPresented: $vm.isShowingDetail){
                    TaskDetailView(task: vm.selectedTask!)
                        .onDisappear {
                            vm.setup(user)
                        }
                }
                
                .sheet(isPresented: $vm.isShowingActiveView, onDismiss: { vm.setup(user) }, content: {
                    ActiveTaskView(task: activeVM.activeTask!)
                })
                
            }
        }
    }
    
    @ViewBuilder
    func allTasksView() -> some View {
        Section("To-do") {
            ForEach(vm.user.tasks) {task in
                if !task.isCompleted && activeVM.activeTask != task && !task.isExpired {
                    TaskCell(task: task)
                }
            }
            
        }
    }
    
    @ViewBuilder
    func dailyTasksView() -> some View {
        Section("Today's Tasks") {
            ForEach(vm.user.tasks.filter { task in
                if let startTime = task.startTime {
                    return Calendar.current.isDateInToday(startTime)
                } else {
                    return false
                }
            }) { task in
                if !task.isCompleted && activeVM.activeTask != task && !task.isExpired {
                    TaskCell(task: task)
                }
            }
        }
    }
    
    
    @ViewBuilder
    func weeklyTasksView() -> some View {
        let dateFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .none
            return formatter
        }()
        
        let thisWeekTasksByDate = (0..<7).compactMap { index -> (Date, [focusAssistant.Task])? in
            let date = Calendar.current.date(byAdding: .day, value: index, to: Date()) ?? Date()
            let tasksForDay = vm.user.tasks.filter { task in
                if let startTime = task.startTime {
                    return Calendar.current.isDate(startTime, inSameDayAs: date)
                } else {
                    return false
                }
            }
            return tasksForDay.isEmpty ? nil : (date, tasksForDay)
        }
        
        ForEach(thisWeekTasksByDate, id: \.0) { date, tasksForDay in
            Section(header: Text("\(date, formatter: dateFormatter)")) {
                ForEach(tasksForDay) { task in
                    if !task.isCompleted && activeVM.activeTask != task && !task.isExpired {
                        TaskCell(task: task)
                    }
                }
            }
        }
    }
    
    @ViewBuilder
    func TaskCell(task: focusAssistant.Task) -> some View {
        
        
        HStack(spacing: 20) {
            Image(systemName: task.imageURL ?? "note.text")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
                .foregroundStyle(.white)
            
            VStack(alignment: .leading) {
                Text(task.name)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .foregroundStyle(.white)
                    .frame(alignment: .leading)
                Text(task.duration.toString)
                    .foregroundStyle(.white)
            }
            Spacer()
            
            if task.pomodoro {
                Image(systemName: "repeat.circle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 20)
                    .foregroundStyle(.white)
                Text(String(task.pomodoroCounter!))
                    .foregroundStyle(.white)
            } else {
                WeightingIndicator(weight: task.priority)
                    .frame(alignment: .trailing)
            }
            
        }
        .contentShape(Rectangle())
        .onTapGesture {
            vm.selectedTask = task
            vm.isShowingDetail = true
        }
        .listRowBackground(
            ZStack {
                if task.priority == .high {
                    LinearGradient(colors: [.faPurple, priorityAnimator ? .red : .faPurple], startPoint: .topLeading, endPoint: .bottomTrailing)
                    .animation(.easeInOut(duration: 2), value: priorityAnimator)
                    
                } else if task.blended {
                    Color.darkPurple
                } else {
                    Color.faPurple
                }
            }
                
                
        )
        .onAppear {
            if task.priority == .high {
                Timer.scheduledTimer(withTimeInterval: 1.25, repeats: true) { timer in
                    priorityAnimator.toggle()
                }
            }
        }
        .swipeActions() {
            Button(role: .destructive) { vm.user.deleteTask(task) } label: {
                Label("Delete", systemImage: "trash")
            }
            if !task.blended {
                Button(action: {
                    vm.selectedTask = task
                    vm.isDisplayingEditView = true
                }, label: {
                    Label("Edit", systemImage: "square.and.pencil")
                })
            }
        }
    }
    
    @ViewBuilder
    func ActiveTaskCell(task: focusAssistant.Task) -> some View {
        
        HStack(spacing: 20) {
            Image(systemName: task.imageURL ?? "note.text")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
                .foregroundStyle(.white)
            
            VStack(alignment: .leading) {
                Text(task.name)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .foregroundStyle(.white)
                    .frame(alignment: .leading)
                Text(activeVM.timerStringValue)
                    .foregroundStyle(.white)
            }
            Spacer()
            
            Image(systemName: "deskclock.fill")
                .resizable()
                .frame(width: 20, height: 20)
                .foregroundStyle(.white)
            
        }
        
        .contentShape(Rectangle())
        .onAppear {
            Timer.scheduledTimer(withTimeInterval: 0.75, repeats: true) { timer in
                activeAnimator.toggle()
            }
        
        }
        .onTapGesture {
            vm.isShowingActiveView = true
        }
        .listRowBackground(
            ZStack {
                Rectangle()
                    .fill(Color.faPurple)
                    .opacity(activeAnimator ? 1 : 0)
                
                Rectangle()
                    .fill(Color.activeFaPurple)
                    .opacity(activeAnimator ? 0 : 1)
                
            }
                .animation(.easeInOut(duration: 0.75), value: activeAnimator)
        )
        .swipeActions() {
            Button(role: .destructive) {
                activeVM.endTimer()
                vm.user.deleteTask(task)
            } label: {
                Label("Delete", systemImage: "trash")
            }
        }
        
        
    }
    
    @ViewBuilder
    func BlendedTaskCell(blendedTask: BlendedTask) -> some View {
        let task = blendedTask.task
        
        HStack(spacing: 20) {
            Image(systemName: task.imageURL ?? "tornado")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
                .foregroundStyle(.white)
            
            VStack(alignment: .leading) {
                Text(task.name)
                    .font(.title2)
                    .fontWeight(.semibold)
                    .foregroundStyle(.white)
                    .frame(alignment: .leading)
                Text(task.duration.toString)
                    .foregroundStyle(.white)
            }
            
            Spacer()
            
            if task.pomodoro {
                Image(systemName: "repeat.circle")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 20)
                    .foregroundStyle(.white)
                Text(String(task.pomodoroCounter!))
                    .foregroundStyle(.white)
            } else {
                WeightingIndicator(weight: task.priority)
                    .frame(alignment: .trailing)
            }
            
        }
        .contentShape(Rectangle())
        .onTapGesture {
            vm.selectedTask = task
            vm.isShowingDetail = true
        }
        .listRowBackground(
            ZStack {
                Color.darkPurple
            }
                
                
        )
        .onAppear {
            Timer.scheduledTimer(withTimeInterval: 1.25, repeats: true) { timer in
                priorityAnimator.toggle()
            }
        }
        .swipeActions() {
            Button(role: .destructive) { vm.user.deleteBlendedTask(blendedTask) } label: {
                Label("Delete", systemImage: "trash")
            }
            
        }
    }
}


#Preview {
    TaskView()
        .environment(mockUser)
        .environmentObject(ActiveTaskViewModel(activeTask: mockTask))
    
}
