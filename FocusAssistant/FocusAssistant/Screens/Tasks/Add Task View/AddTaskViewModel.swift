//
//  AddTaskViewModel.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 01/03/2024.
//

import SwiftUI

@Observable class AddTaskViewModel {
    
    var task = focusAssistant.Task()
    var user = User().readData()
    var alertItem: AlertItem?
    var isShowingDurationPicker = false
    var isShowingIconPicker = false
    
    var isComplete: Bool {
        if task.pomodoro { return task.name != "" } else { return task.name != "" && !task.duration.isEmpty() && task.startTime != nil }
    }
    
    func setup(_ user: User) {
        self.user = user.readData()
    }
}
