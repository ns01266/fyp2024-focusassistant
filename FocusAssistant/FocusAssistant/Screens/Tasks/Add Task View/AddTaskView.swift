//
//  AddTaskView.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 29/02/2024.
//

import SwiftUI
import SymbolPicker

struct AddTaskView: View {
    
    @Environment(User.self) var user
    @Bindable var vm = AddTaskViewModel()
    @FocusState private var isFocused: Bool
    @Environment(\.dismiss) private var dismiss
    
    var newTask = true
    @State var task: focusAssistant.Task?
    
    init() {}
    
    init(task: focusAssistant.Task){
        self.task = task
        self.newTask = false
        self.vm.task = task
    }
    
    
    var body: some View {
        
        NavigationStack {
            ZStack {
                Color.BG
                    .ignoresSafeArea()
                Form {
                    Section {
                        TextField(text: $vm.task.name) {Text("Name")}
                            .focused($isFocused)
                            .autocorrectionDisabled(true)
                        
                        
                        Toggle(isOn: $vm.task.pomodoro, label: {
                            Text("Pomodoro Task?")
                        })
                        .onChange(of: vm.task.pomodoro) { oldValue, newValue in
                            if newValue == true {
                                vm.task.pomodoroCounter = 0
                                vm.task.duration = Duration(secondsComponent: 300*300, attosecondsComponent: 0)
                                vm.task.startTime = nil
                            }
                        }
                        if !vm.task.pomodoro {
                            DatePicker("Start Time",
                                       selection: $vm.task.startTime.bound,
                                       displayedComponents: [.date, .hourAndMinute])
                            
                            Button(vm.task.duration.isEmpty() ? "Duration" : "Duration: \(vm.task.duration.toString)") { vm.isShowingDurationPicker = true }
                            
                                .sheet(isPresented: $vm.isShowingDurationPicker) {
                                    DurationPicker(isShowing: $vm.isShowingDurationPicker, duration: $vm.task.duration)
                                }
                        }
                        
                        Picker(selection: $vm.task.priority) {
                            ForEach(Priority.allCases) { priority in
                                Text(priority.rawValue.capitalized)
                            }
                        } label: {
                            Text("Priority")
                        }
                        
                        
                        
                    } header: { Text("Task Details") }
                    
                    Section {
                        Button {
                            vm.isShowingIconPicker = true
                        } label: {
                            HStack {
                                Text(vm.task.imageURL == nil ? "Choose Icon" : "Change icon")
                                Spacer()
                                Image(systemName: vm.task.imageURL ?? "square.and.arrow.up")
                                    .frame(alignment: .trailing)
                            }
                            
                        }
                        .sheet(isPresented: $vm.isShowingIconPicker) {
                            SymbolPicker(symbol: $vm.task.imageURL)
                        }
                        
                        TextField(text: $vm.task.details.bound) {
                            Text("Describe the task")
                        }
                        .focused($isFocused)
                        
                        
                        
                    } header: {Text("Optional Details") }
                    
                    Section {
                        Button {
                            if !vm.isComplete {
                                vm.alertItem = AlertContext.invalidForm
                                return
                            }
                            
                            vm.user.checkTimeClash(vm.task)
                            guard vm.user.alertItem == nil else {
                                vm.alertItem = vm.user.alertItem
                                return
                            }
                            
                            if newTask {
                                let newTask = focusAssistant.Task()
                                vm.task.id = newTask.id
                                vm.user.addTask(vm.task)
                            } else {
                                vm.user.updateTask(vm.task)
                            }
                            
                            dismiss()
                            
                        } label: {
                            Text(newTask ? "Create Task" : "Save changes")
                        }
                    }
                    
                }
                .toolbar {
                    ToolbarItemGroup(placement: .keyboard) {
                        Button("Dismiss") { isFocused = false }
                    }
                    
                }
                .scrollContentBackground(.hidden)
                
            }
            
            .navigationTitle(newTask ? "New Task" : "Edit Task")
            .toolbar {
                ToolbarItem(placement: .topBarLeading) {
                    Button {
                        dismiss()
                    } label: { Text("Cancel") }
                }
            }
        }
        .onAppear {
            vm.setup(user)
        }
        .alert(item: $vm.alertItem) { alertItem in
            Alert(title: alertItem.title,
                  message: alertItem.message,
                  dismissButton: alertItem.dismissButton)
        }
        
        
    }
}

#Preview {
    AddTaskView().environment(User())
}


