//
//  Alert.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 02/03/2024.
//

import SwiftUI

struct AlertItem: Identifiable, Equatable {
    static func == (lhs: AlertItem, rhs: AlertItem) -> Bool {
        return lhs.message == rhs.message
    }
    
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton : Alert.Button
}

struct AlertContext {
    static let invalidData = AlertItem(title: Text("Server Error"),
                                              message: Text("The Data received from the server was invalid"),
                                              dismissButton: .default((Text("OK"))))
    
    static let invalidResponse = AlertItem(title: Text("Server Error"),
                                              message: Text("Invalid response from the server. Please try again later"),
                                              dismissButton: .default((Text("OK"))))
    
    static let invalidURL = AlertItem(title: Text("Server Error"),
                                              message: Text("There was an issue connecting to the server"),
                                              dismissButton: .default((Text("OK"))))
    
    static let unableToComplete = AlertItem(title: Text("Server Error"),
                                              message: Text("Unable to complete your request at this time. Please check your network connection"),
                                              dismissButton: .default((Text("OK"))))
    
    static let invalidForm = AlertItem(title: Text("Invalid Form"),
                                              message: Text("Please ensure all fields in the form have been filled out"),
                                              dismissButton: .default((Text("OK"))))

    static let invalidEmail = AlertItem(title: Text("Email Error"),
                                              message: Text("The email you inputted is invalid. PLease try again"),
                                              dismissButton: .default((Text("OK"))))
    
    static let userSaveSuccess = AlertItem(title: Text("Profile Saved"),
                                              message: Text("Your profile information was successfully saved."),
                                              dismissButton: .default((Text("OK"))))
    
    static let invalidUserData = AlertItem(title: Text("Profile Error"),
                                              message: Text("There was an error saving or retrieving your profile."),
                                              dismissButton: .default((Text("OK"))))
    
    static let profileCreationSuccess = AlertItem(title: Text("Profile Created"),
                                              message: Text("Profile was successfully created"),
                                              dismissButton: .default((Text("OK"))))
    
    static let profileEditSuccess = AlertItem(title: Text("Profile Edited"),
                                              message: Text("Profile was successfully modified"),
                                              dismissButton: .default((Text("OK"))))
    
    func taskClash(_ existingTask: focusAssistant.Task) -> AlertItem {
        return AlertItem(title: Text("Task Clashing"),
                         message: Text("The task you are adding starts when \(existingTask.name) is set to be running"),
                         dismissButton: .default(Text("OK")))
    }
}

