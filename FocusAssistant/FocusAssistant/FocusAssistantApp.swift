//
//  FocusAssistantApp.swift
//  FocusAssistant
//
//  Created by Nana Sekyere on 27/02/2024.
//

import SwiftUI

@main
struct FocusAssistantApp: App {
    
    @State private var profile = User().readData()
    @State private var activeTaskModel: ActiveTaskViewModel = .init()
    @State private var activeTaskIDs: Set<UUID> = []
    
    @Environment(\.scenePhase) var phase
    @State var lastActiveTimeStamp: Date?
    
    init() {
        UITableView.appearance().backgroundColor = .clear
    }
    var body: some Scene {
        WindowGroup {
            FocusAssistantTabView()
                .environment(\.scenePhase, phase)
                .onAppear {
                    startBackgroundTask()
                }
                .onReceive(activeTaskModel.timer) { _ in
                    if activeTaskModel.isStarted && !activeTaskModel.isShowing {
                        activeTaskModel.updateTimer()
                    }
                }
                .onChange(of: activeTaskModel.activeTask, { oldValue, newValue in
                    if newValue == nil {
                        print("success")
                    }
                })
                .environment(profile)
                .environmentObject(activeTaskModel)
            
        }
        
        .onChange(of: phase) { oldValue, newValue in
            if activeTaskModel.isStarted {
                if newValue == .background {
                    lastActiveTimeStamp = Date()
                }
                
                if newValue == .active {
                    if let timeStamp = lastActiveTimeStamp {
                        let currentTimeStampDiff = Date().timeIntervalSince(timeStamp)
                        if activeTaskModel.totalSeconds - Int(currentTimeStampDiff) <= 0 {
                            activeTaskModel.isStarted = false
                            activeTaskModel.totalSeconds = 0
                            activeTaskModel.isFinished = true
                        } else { activeTaskModel.totalSeconds -= Int(currentTimeStampDiff) }
                    }
                }
            }
        }
        
    }
    
    private func startBackgroundTask() {
        DispatchQueue.global(qos: .background).async {
            while true {
                self.checkHighPriorityTasks()
                self.checkExpiredTasks()
                // Sleep for 15 seconds before checking again
                Thread.sleep(forTimeInterval: 15)
            }
        }
    }
    
    private func checkHighPriorityTasks() {
        let user = profile.readData()
        
        
        for task in user.tasks where task.priority == .high {
            if let startTime = task.startTime, Calendar.current.isDate(Date(), equalTo: startTime, toGranularity: .minute) {
                print("task should start now")
                if !activeTaskIDs.contains(task.id) {
                    DispatchQueue.main.async {
                        print("Starting Task")
                        // Set the task as the active task
                        activeTaskModel.setActiveTask(task)
                        activeTaskIDs.insert(task.id)
                        // Start the timer
                        activeTaskModel.startTimer()
                        // Notify the user
                        notifyUser(for: task)
                    }
                }
            }
        }
    }
    
    private func checkExpiredTasks() {
        let user = profile.readData()
        let currentTime = Date.now
        
        for var task in user.tasks where !task.isCompleted && !task.pomodoro {
            if task.startTime! < currentTime {
                task.isExpired = true
                profile.updateTask(task)
            }
        }
    }
    
    
    private func notifyUser(for task: focusAssistant.Task) {
        let content = UNMutableNotificationContent()
        content.title = "High Priority Task Timer Started"
        content.body = "Your high priority task timer for \(task.name) has started."
        content.sound = UNNotificationSound.default
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request)
    }
}
