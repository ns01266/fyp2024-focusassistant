import UIKit

import GoogleGenerativeAI

// For text-and-image input (multimodal), use the gemini-pro-vision model
// Access your API key from your on-demand resource .plist file (see "Set up your API key" above)
let model = GenerativeModel(name: "gemini-pro", apiKey: AIzaSyC26bdSGShMvmkuI4auAO3hNxOroqygWWI)

let image1 = UIImage(named: "")!
let image2 = UIImage(named: "")!

let prompt = "What's different between these pictures?"
var fullResponse = ""
let contentStream = model.generateContentStream(prompt, image1, image2)
for try await chunk in contentStream {
  if let text = chunk.text {
    print(text)
    fullResponse += text
  }
}
print(fullResponse)
